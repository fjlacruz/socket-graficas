import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebsocketService } from '../../services/websocket.service';
import { Color, BaseChartDirective, Label } from 'ng2-charts';

@Component({
  selector: 'app-grafica',
  templateUrl: './grafica.component.html',
  styleUrls: ['./grafica.component.css']
})
export class GraficaComponent implements OnInit {
  public lineChartData: Array<any> = [{ data: [0, 0, 0, 0], label: 'Ventas' }];
  public lineChartLabels: Array<any> = ['Enero', 'Febrero', 'Marzo', 'Abril'];
  public lineChartColors: Color[] = [
    // {
    //   // grey
    //   backgroundColor: 'rgba(148,159,177,0.2)',
    //   borderColor: 'rgba(148,159,177,1)',
    //   pointBackgroundColor: 'rgba(148,159,177,1)',
    //   pointBorderColor: '#000',
    //   pointHoverBackgroundColor: '#000',
    //   pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    // },
    {
      // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#000',
      pointHoverBackgroundColor: '#000',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    }
    // {
    //   // red
    //   backgroundColor: 'rgba(255,0,0,0.3)',
    //   borderColor: 'red',
    //   pointBackgroundColor: 'rgba(148,159,177,1)',
    //   pointBorderColor: '#000',
    //   pointHoverBackgroundColor: '#000',
    //   pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    // }
  ];

  constructor(private http: HttpClient, public wsService: WebsocketService) {}

  ngOnInit() {
    this.getData();
    this.escucharSocket();
  }

  getData() {
    this.http
      .get('http://localhost:5000/grafica')
      .subscribe((data: any) => (this.lineChartData = data));
  }

  escucharSocket() {
    this.wsService.listen('cambio-grafica').subscribe((data: any) => {
      console.log('socket', data);
      this.lineChartData = data;
    });
  }
}
